export default async function authentication(username:string, password:string){
    const form = new FormData();
    form.append('username',username)
    form.append('password',password)

    const response = await fetch('http://localhost:8000/api/users/authenticate/',
        {
            method: 'POST',
            body: form
        })
        const token = await response.json()
        return token
}