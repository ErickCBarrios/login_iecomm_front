import {Button} from "../../../../types";
import style from './styles/buttonGen.module.css'

const ButtonGen = ({
  buttonName,
  color,
  text_color,
  border,
  width,
  buttonFunction,
}: Button) => {
  const buttonStyle = {
    backgroundColor: color,
    color: text_color,
    border: border,
    width: width,
  };

  return (
    <>
      <button
        style={buttonStyle}
        className={style.buttonGen}
        type="submit" 
        onClick={() => {
          buttonFunction();
        }}
      >
        {" "}{buttonName}{" "}
      </button>
    </>
  );
};
export default ButtonGen;
