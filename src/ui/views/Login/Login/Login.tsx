import {LoginCard} from '../../../components/index'
import style from '../styles/login.module.css'

export const Login = () => {
  return (
    <div className={style.login}>
      <LoginCard/>
    </div>
  )
}
export default Login;