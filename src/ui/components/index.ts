import FontPage from "./Login/FontPage";
import LoginCard from "./Login/LoginCard";
import LoginForm from "./Login/LoginForm";

export{
    LoginCard,
    FontPage,
    LoginForm,
}