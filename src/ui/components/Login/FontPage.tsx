import imgLogin from '../../../assets/img/ImgLogin.jpg'
import style from './styles/fontpage.module.css'

const FontPage = () => {
  return (
    <div className={style.content}>
        <img className={style.img} src={imgLogin} alt="Imagen de login" />
    </div>
  )
}
export default FontPage;