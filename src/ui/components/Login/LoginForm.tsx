import React, {useRef, useState} from 'react'

import ButtonGen from '../../generator/Buttons/ButtonGen'
import { authentication } from '../../../services'
import style from './styles/loginform.module.css'

export const LoginForm = () => {


    
    const usernameInput = useRef<HTMLInputElement>(null);
    const passwordInput = useRef<HTMLInputElement>(null);
    const [error, setError] = useState({
        errorUser: '',
        errorPassword: '',
    })
    const Authenticacion_login = (event:React.FormEvent<HTMLFormElement>) => {
        if (event !== undefined){ //verificar que el evento no venga vacio
            event.preventDefault(); //para que cuando se de click o enter no se refresque automaticamente la pagina
            const username = usernameInput.current?.value //se pasa la referencia del valor del input a una variable para mejorar su manejo
            const password = passwordInput.current?.value


            if(username === '' && password === ''){
                setError({...error,
                    errorUser: 'The field username is required',
                    errorPassword: 'The field password is required'
                })
                return error
            }
            if(username === '' && password !== null){
                setError({...error,
                    errorUser: 'The field username is required',
                    errorPassword: ''
                })
                return error
            }
            if(username !== null && password === ''){
                setError({...error,
                    errorUser: '',
                    errorPassword: 'The field password is required'
                })
                return error
            }
            if( username !== null && password !== null){
                authentication(username as string,password as string).then(res => {
                    if(res.detail !== undefined){
                        console.log(res.detail)
                    }else{
                        console.log(res.user)
                    }
                })

            }
            setError({...error,
                errorUser: '',
                errorPassword: ''
            })
            
        }
    }
  return (
    <div className={style.content}>
        <div className={style.content_title}>
            <h1 >Login</h1> 
            <p>Bienvenid@s</p>
        </div>
        <form action="" onSubmit={
             (event) => {
                Authenticacion_login(event as React.FormEvent<HTMLFormElement>)
            }

        }>
            <div className={style.item_input}>
                <label htmlFor="email">Username</label>
                    <input 
                        ref={usernameInput} 
                        type="text" 
                        name="username" 
                        id="username" 
                        placeholder='Username'
                    />
                {error && <span>{error.errorUser}</span>}    
            </div>
            <div className={style.item_input}>
                <label htmlFor="password">Password</label>
                <input 
                    ref={passwordInput} 
                    type="text" 
                    name="password" 
                    id="password" 
                    placeholder='*************' />
                {error && <span>{error.errorPassword}</span>}     
            </div>    
            <div className={style.containerButton}>
                <ButtonGen buttonName='Log in' buttonFunction={
                  (event) => {
                    event?.preventDefault();
                    Authenticacion_login(event as React.FormEvent<HTMLFormElement>)
                }

                }/>
                <ButtonGen buttonName='Register' color='#333333' buttonFunction={
                    (event) => {
                        console.log('te logeaste')
                        event?.preventDefault();
                    }   
                }/>
            </div>
            <div className={style.no_accountContent}>
                <span>Hello, ¿Forgot password?</span>
            </div>
        </form>
    </div>
  )
}
export default LoginForm