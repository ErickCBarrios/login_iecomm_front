import {FontPage, LoginForm} from '../index'

import style from './styles/logincard.module.css'

const LoginCard = () => {
  return (
    <div className={style.content}>
        <FontPage />
        <LoginForm />
    </div>
  )
}
export default LoginCard