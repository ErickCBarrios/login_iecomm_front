export interface Button {
    buttonName: string;
    color?: string;
    text_color?: string;
    border?: string;
    width?: string;
    buttonFunction: (event?:React.FormEvent<HTMLFormElement>) => void;
  
}